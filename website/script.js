
let sites = document.getElementsByClassName("site");

let c = 0;


for (var a=[],i=0;i<sites.length;++i) a[i]=i;

// http://stackoverflow.com/questions/962802#962890
function shuffle(array) {
	var tmp, current, top = array.length;
	if(top) while(--top) {
		current = Math.floor(Math.random() * (top + 1));
		tmp = array[current];
		array[current] = array[top];
		array[top] = tmp;
	}
	return array;
}

a = shuffle(a);

let searchInput = document.getElementById("searchInput");

Array.prototype.forEach.call(sites, function(site) {
	site.addEventListener("click", function(){
		location = site.dataset.url.replace(
			"%s",
			encodeURIComponent(searchInput.value)
		);
	});
	site.style.order = a[c];
	c++;
});

window.addEventListener('load', function(){
	searchInput.focus();
});
